package com.example.demo.config;

import com.example.demo.domain.tables.daos.ClientDao;
import org.jooq.DSLContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DaoConfig {

    @Bean
    public ClientDao clientDao(DSLContext dslContext) {
        return new ClientDao(dslContext.configuration());
    }
}
