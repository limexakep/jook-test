package com.example.demo.repository;

import java.util.*;

import com.example.demo.domain.tables.daos.ClientDao;
import com.example.demo.domain.tables.pojos.Client;
import com.example.demo.domain.tables.records.*;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ClientRepository {

    private final DSLContext dslContext;

    private final ClientDao clientDao;

    public Optional<Client> getClientById(Long id) {
        return dslContext.selectFrom(com.example.demo.domain.tables.Client.CLIENT)
            .where(com.example.demo.domain.tables.Client.CLIENT.ID.eq(id))
            .fetchOptional()
            .map(clientDao.mapper()::map);
    }

    public List<Client> getAll() {
        List<Client> clients = new ArrayList<>();

        dslContext.select()
            .from(com.example.demo.domain.tables.Client.CLIENT)
            .fetch()
            .forEach(rec -> clients.add(
                    new Client(
                        rec.get(com.example.demo.domain.tables.Client.CLIENT.ID),
                        rec.get(com.example.demo.domain.tables.Client.CLIENT.NAME),
                        rec.get(com.example.demo.domain.tables.Client.CLIENT.EMAIL)
                    )
                )
            );

        return clients;
    }

}
