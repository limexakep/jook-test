package com.example.demo.service;

import com.example.demo.domain.tables.pojos.Client;
import com.example.demo.repository.ClientRepository;
import lombok.*;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;

    public List<Client> getClients() {
        return clientRepository.getAll();
    }

    public Client getClientById(Long id) {
        return clientRepository.getClientById(id).get();
    }

}
